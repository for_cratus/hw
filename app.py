from flask import Flask, render_template, request, redirect, url_for, jsonify
from flask_cors import CORS
from werkzeug.utils import secure_filename
import cv2
import os
import uuid
import sys
from yolov3_lib import prediction

print(sys.version)

app = Flask(__name__)
CORS(app)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/version')
def version():
    return sys.version

@app.route('/analyze', methods = ['POST'])
def analyze():
    f = request.files['file']
    basepath = os.path.dirname(__file__)
    file_path = os.path.join(basepath, 'uploads', str(uuid.uuid4()))
    f.save(file_path)

    # preds = model_predict(file_path, model)

    # pred_class = preds.argmax(axis=-1)            # Simple argmax
    #pred_class = decode_predictions(preds, top=1)   # ImageNet Decode
    # result = str(pred_class[0][0][1])               # Convert to string
    im = cv2.imread(file_path)
    boxes = prediction.processImage(im)
    print(boxes)
    response = [{'min_x': b[0], 'max_x': b[0] + b[2], 'min_y': b[1], 'max_y': b[1] + b[3]} for b in boxes]
    
    return jsonify(response)

